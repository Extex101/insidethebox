
(C) 2016-2018
  `Auke Kok <sofar@foo-projects.org>`
  `Nathanael Courant <nathanael.courant@laposte.net>`

Licensing Terms and Conditions
==============================

Insidethebox (itb) is licensed permissively, generally under the LGPL-2.1
license, or (at your option) any later version, for all itb code.

ITB Artwork is CC-BY-4.0.

ITB contains code and artwork that is borrowed from other mods and may
be licensed under different license terms. You should confirm that your
use of the code and artwork is compatible with these licenses. These
licenses are stated in the subfolders and files of these components
where relevant.

Most importantly, most of the textures are used from, and modified
from, the Isabella-II texture pack, and licensed "Public Domain" by
`bonemouse` (See: https://minecraft.curseforge.com/projects/isabella-ii)

Sounds are largely from FreeSound.org. They vary in license from CC0
to CC-BY-SA-4.0. The music score is kept out of the git tree for
copyright reasons. The official ITB server uses -ND -NC music tracks,
and we did not want to commit them into git.

Maintaining proper licensing statements and attributes is important to
us. If you see inconsistencies or errors, please let us know.

~~~~

    ITB (insidethebox) minetest game - Copyright (C) 2016-2018 sofar & nore

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation; either version 2.1
    of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free
    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
    MA 02111-1307 USA

~~~~

For the full text of the CC-BY-SA-4.0 text, see:
    `https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt`
