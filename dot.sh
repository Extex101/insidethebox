(
  echo "digraph {"
  grep -r . mods/*/depends.txt | sed 's/mods.//g ; s/.depends.txt:/ -\> /g ; s/?//g ; s/$/;/ '
  echo "}"
) | dot -Tsvg
