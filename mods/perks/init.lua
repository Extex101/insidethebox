
--[[

  perks mod for minetest

  Copyright (C) 2019 Auke Kok <sofar@foo-projects.org>

  Permission to use, copy, modify, and/or distribute this software for
  any purpose with or without fee is hereby granted, provided that the
  above copyright notice and this permission notice appear in all copies.

  THE SOFTWARE IS PROVIDED "AS IS" AND ISC DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL ISC BE LIABLE FOR ANY
  SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING
  OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

]]--

perks = {}

function perks.grant(name_or_player)
	local name
	local player

	if type(name_or_player) == "string" then
		name = name_or_player
		player = minetest.get_player_by_name(name)
	elseif type(name_or_player) == "userdata" then
		player = name_or_player
		name = player:get_player_name()
	else
		return
	end

	if not player then
		-- offline players - defer granting of perk
		return
	end

	-- count all boxes built by name
	local accepted = 0
	for k, v in pairs(boxes.get_player_boxes(name)) do
		if v.status == db.STATUS_ACCEPTED and v.builder == name then
			accepted = accepted + 1
		end
	end

	if accepted >= 2 then
		local privs = minetest.get_player_privs(name)
		if not privs.zoom then
			privs.zoom = true
			minetest.set_player_privs(name, privs)
			minetest.log("perks: granted zoom to " .. name)
			announce.all(name .. " has been granted the zoom perk!")
			telex.send({
				to = name,
				from = "sofar",
				subject = "Congratulations! You've been awarded a perk!",
				content = {
					"Dear player,",
					" ",
					"We really appreciate the hard work you put in building boxes.",
					" ",
					"As a thank you, you have been granted the \"zoom\" perk!",
					" ",
					"--sofar"
				}
			})
		end
	end

	if accepted >= 3 then
		local limit = tonumber(player:get_attribute("box_create_limit") or "3")
		if limit <= 3 then
			minetest.log("perks: granted more boxes to " .. name)
			announce.all(name .. " has been granted the more boxes perk!")
			telex.send({
				to = name,
				from = "sofar",
				subject = "Congratulations! You've been awarded a perk!",
				content = {
					"Dear player,",
					" ",
					"We really appreciate the hard work you put in building boxes.",
					" ",
					"As a thank you, you have been granted the \"more boxes\" perk!",
					" ",
					"--sofar"
				}
			})
			player:set_attribute("box_create_limit", "5")
		end
	end

	if accepted >= 5 then
		local privs = minetest.get_player_privs(name)
		if not privs.sprint then
			privs.sprint = true
			minetest.set_player_privs(name, privs)
			minetest.log("perks: granted sprint to " .. name)
			announce.all(name .. " has been granted the sprint perk!")
			telex.send({
				to = name,
				from = "sofar",
				subject = "Congratulations! You've been awarded a perk!",
				content = {
					"Dear player,",
					" ",
					"We really appreciate the hard work you put in building boxes.",
					" ",
					"Five boxes is an amazing milestone, and we are truly thankful for it.",
					" ",
					"As a thank you, you have been granted the \"sprint\" perk!",
					" ",
					"--sofar"
				}
			})
		end
	end
end

minetest.register_on_joinplayer(function(player)
	perks.grant(player)
end)
