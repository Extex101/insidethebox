`mech_humm_60hz.ogg`
- CC-BY-3.0 Auke Kok <sofar@foo-projects.org>

`fire_fire.*.ogg`:
- http://freesound.org/people/jan.bockelson/sounds/342104/ CC0

`water_flowing.ogg`:
- http://freesound.org/people/forrisday/sounds/167365/ CC-BY-3.0

`leaves.ogg`:
- http://freesound.org/people/lunchmoney/sounds/378725/ CC0

`grass_dig.*.ogg`:
- https://freesound.org/people/OwlStorm/packs/9344/ Natalie Kirk CC-BY-3.0

`gravel_dig.*.ogg`:
- http://freesound.org/people/Ali_6868/packs/21608/ CC0

`sand_dig.*.ogg`:
- http://freesound.org/people/Ali_6868/sounds/384362/ CC0

`snow_dig.*.ogg`:
- http://freesound.org/people/Henrythetrain/sounds/174508/ CC0

`leaves_step.*.ogg`:
- http://freesound.org/people/Anthousai/sounds/398776/ CC0

`glass_dig.*.ogg`:
- https://freesound.org/people/unfa/sounds/221528/ CC-BY-3.0
- https://freesound.org/people/jorickhoofd/sounds/179229/ CC-BY-3.0
- https://freesound.org/people/nick121087/sounds/232176/ CC0

`cool_lava.*.ogg`:
- Natalie - http://www.pdsounds.org/sounds/water_vaporizing_on_hot_stones CC0

`dig_dig_immediate.ogg`, `dig_oddly_breakable_by_hand.ogg`:
- Mito551 - CC-BY-SA

`metal_dig.*.ogg`:
- qubodup - CC0 https://freesound.org/people/qubodup/sounds/54850/

`item_smoke.ogg`:
- bart - CC0 - http://opengameart.org/users/bart

`cloth_*.ogg`,
All other `_dig`, and `_step` sounds:
- SnowSong - (C) Sn0wShepherd / Alecia Shepherd! - CC-BY-alike -
"I herein give permission to use my SnowSong Sound Pack free of charge, entirely free, for personal enjoyment, modification of the Minecraft gaming experience, and most importantly, for Foley and specialized usage in your OWN MODS, resource packs, and your own GAMES if you are a game designer; INDIE or otherwise."
